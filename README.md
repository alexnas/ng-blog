# Angular Blog

This Angular application for creation and reading of blog posts. Admin interface is provided for blog post creation.

## Uses

- Angular 8.0
- Angular Forms
- rxjs
- ngx-quill
- Firebase API on the backend
- Login/password authentication through Firebase API
- Firebase data storage

## Installation and Usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

- Open app at localhost:4200 in a browser
- Open admin app at localhost:4200/admin in a browser (login: aaa@aaa.aa, pass: aaaaaaaa)

## License

Under the terms of the MIT license.
